from argparse import ArgumentParser
import os

import ase.db
import numpy as np
from tqdm import tqdm


def main(args): #pylint: disable=redefined-outer-name
    assert args.qm9_path.endswith(".db"), "database path must end with .db"
    assert args.qm9_force_path.endswith(".db"), "database path must end with .db"


    with ase.db.connect(args.qm9_path) as db, ase.db.connect(args.qm9_force_path) as force_db:
        for row in tqdm(db.select()):
            data = zero_force_data(row)
            force_db.write(row.toatoms(), data=data)


def zero_force_data(row):
    data = row.data
    data["forces"] = np.zeros_like(row.positions)
    return data


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--qm9-path", default=os.path.expanduser("~/storage/qm9.db"))
    parser.add_argument("--qm9-force-path", default=os.path.expanduser("~/storage/qm9-force.db"))
    args = parser.parse_args()

    main(args)
